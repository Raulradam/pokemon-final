/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicapokmon;

import practicapokmon.Pokemon;
import java.util.ArrayList;
import practicapokmon.Items;
import practicapokmon.Pokeball;
/**
 * 
 * @author dam
 */
public class Mochila {

    protected int dinero;
    protected ArrayList<Pokemon> Mochila;
    protected int pokeballs;
    //protected int pociones;
/**
 * 
 */
    public Mochila() {
        this.dinero = 500;
        Mochila = new ArrayList<>();

    }
/**
 * 
 * @param numPoke
 * @return 
 */
    public String mostrarPokemon(int numPoke) {
        String lista;
        lista = " Tu pokemon ";
        lista = lista + Mochila.get(numPoke).getNombre();
        lista = lista + " su nivel es ";
        lista = lista + Mochila.get(numPoke).getNivel();
        lista = lista + " de la especie ";
        lista = lista + Mochila.get(numPoke).getEspecie();

        return lista;
    }
/**
 * 
 * @param posicion
 * @return 
 */
    public String listarPokemon(int posicion) {
        String lista;
        lista = Mochila.get(posicion).getNombre();
        lista = lista + " NV ";
        lista = lista + Mochila.get(posicion).getNivel();

        return lista;
    }
/**
 * 
 * @param posicionPoke 
 */
    public void liberarPokemon(int posicionPoke) {

        this.Mochila.remove(posicionPoke);

    }
/**
 * 
 * @param poke 
 */
    public void registrarPokemon(Pokemon poke) {
        Mochila.add(poke);
    }
/**
 * 
 * @param cantidad
 * @return 
 */
    public boolean comprarPokeball(int cantidad) {
        boolean dineroSuf = true;
        Pokeball Pokebol = new Pokeball();
        if ((Pokebol.getPrecio() * cantidad) > this.dinero) {
            dineroSuf = false;
        } else {
            pokeballs = pokeballs + cantidad;
        }

        return dineroSuf;
    }
  

}
