/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicapokmon;
/**
 * 
 * @author dam Clase squirtle
 */
public class Squirtle extends Pokemon{
    
    /**
     * 
     * @param ps
     * @param ataque
     * @param defensa
     * @param ataqueEsp
     * @param defensaEsp
     * @param velocidad 
     */
    public Squirtle(int ps, int ataque, int defensa, int ataqueEsp, int defensaEsp, int velocidad) { // estats base
        nombre = "SQUIRTLE";
        especie = "SQUIRTLE";
        this.nivel = (int) (Math.random() * 100);
        this.ps= (ps + (nivel * 2));
        this.ataque = (ataque + (nivel * 2));
        this.defensa = (defensa + (nivel * 3));
        this.atqEsp = (atqEsp + (nivel * 2));
        this.defEsp = (defEsp + (nivel * 3));
        this.velocidad = (velocidad + (nivel * 2));
        tipo = "AGUA";
            
    }
    /**
     * Constructor con nivel
     * @param nivel 
     */
    public Squirtle(int nivel) {
        nombre = "SQUIRTLE";
        especie = "SQUIRTLE";
        this.nivel = nivel;
        this.ps= (ps + (nivel * 2));
        this.ataque = (ataque + (nivel * 2));
        this.defensa = (defensa + (nivel * 3));
        tipo = "AGUA";  
    }
    /**
     * 
     * @param nombre 
     */
    public void setNombre(String nombre){
        this.nombre=nombre;
    }
}
