/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicapokmon;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 * 
 * @author dam
 */
public class PracticaPokmon {

  /**
   * 
   * @param args
   * @throws SQLException 
   */
    public static void main(String[] args) throws SQLException {

        Scanner entrada = new Scanner(System.in);
        Mochila bag = new Mochila();
        Jugador player = new Jugador();
        boolean salir = false;
        boolean usuarioRepe=false;
        Statement query;
        int opc = 0;
        int opcp = 0;
        int numPoke;
        int cantidad;
        Connection con;
        ResultSet result;
        String cadena1, cadena2, cadena3;
        do {
            try {
                System.out.println("---PARTIDA POKEMON---");
                System.out.println("1.Nueva partida");
                System.out.println("2.Cargar Partida");
                System.out.println("Elige una opción");
                opcp = entrada.nextInt();
                con = DriverManager.getConnection("jdbc:mysql://192.168.2.109:3306/Pokemon?serverTimezone=UTC", "borjamari", "borjamari");
                switch (opcp) {
                    default:
                        salir = true;
                        break;
                    case 1:  
                        System.out.println("Introduzca su nombre");
                        cadena1 = entrada.next();
                        query = con.createStatement();
                        System.out.println("Introduzca su género");
                        cadena2 = entrada.next();
                        System.out.println("Introduzca su fecha de nacimineto");
                        cadena3 = entrada.next(); 
                        query = con.createStatement();
                        query.executeUpdate("INSERT INTO mochilas VALUE('" + cadena1 + "','" + cadena2 + "',1000,20,'" + cadena3 + "')");
                        result = query.executeQuery("select * from mochilas where nombre='" + cadena1 + "'");

                        while (result.next()) {
                            System.out.println("nombre : " + result.getString(1));
                            System.out.println("genero : " + result.getString(2));
                            System.out.println("pokecuartos : " + result.getInt(3));
                            System.out.println("pokeballs : " + result.getInt(4));
                            System.out.println("fecha naciemiento : " + result.getString(5));
                            player.nombre = result.getString(1);
                            player.genero = result.getString(2);
                            bag.dinero = result.getInt(3);
                            bag.pokeballs = result.getInt(4);
                            player.fecha = result.getDate(5);
                        }
                        break;
                    case 2:
                        //cargar partida
                        
                        System.out.println("Introduzca su nombre");
                        cadena1 = entrada.next();
                        query = con.createStatement();
                        result = query.executeQuery("select * from mochilas where nombre='" + cadena1 + "'");

                        while (result.next()) {
                            System.out.println("nombre : " + result.getString(1));
                            System.out.println("genero : " + result.getString(2));
                            System.out.println("pokecuartos : " + result.getInt(3));
                            System.out.println("pokeballs : " + result.getInt(4));
                            System.out.println("fecha naciemiento : " + result.getString(5));
                            player.nombre = result.getString(1);
                            player.genero = result.getString(2);
                            bag.dinero = result.getInt(3);
                            bag.pokeballs = result.getInt(4);
                            player.fecha = result.getDate(5);
                        }

                        result = query.executeQuery("select * from pokemons where mochila='" + cadena1 + "'");
                        while (result.next()) {
                            System.out.println("especie : " + result.getString(1));
                            System.out.println("nivel : " + result.getInt(2));
                            System.out.println("nombre : " + result.getString(3));
                            if (result.getString(1).equals("PIKACHU")) {
                                Pikachu pika = new Pikachu(result.getInt(2));
                                pika.setNombre(result.getString(3));
                            }
                            if (result.getString(1).equals("CHARMANDER")) {
                                Charmander chari = new Charmander(result.getInt(2));
                                chari.setNombre(result.getString(3));
                            }
                            if (result.getString(1).equals("PIDGEY")) {
                                Pidgey pid = new Pidgey(result.getInt(2));
                                pid.setNombre(result.getString(3));
                            }
                            if (result.getString(1).equals("SQUIRTLE")) {
                                Squirtle squ = new Squirtle(result.getInt(2));
                                squ.setNombre(result.getString(3));
                            }
                        }

                        break;
                }

                do {
                    try {
                        System.out.println("---POKEMON---");
                        System.out.println("1.Listar Pokemon");
                        System.out.println("2.Mostrar Pokemon");
                        System.out.println("3.Liberar Pokemon");
                        System.out.println("4.Capturar Pokemon");
                        System.out.println("5.Comprar Pokeballs");
                        System.out.println("6.Guardar Partida");
                        System.out.println("7.Salir");
                        System.out.println("Elige una opción del menú");
                        opc = entrada.nextInt();
                        switch (opc) {
                            case 1:
                                for (int i = 0; i < bag.Mochila.size(); i++) {
                                    System.out.println((i + 1) + " " + bag.listarPokemon(i));
                                }

                                break;
                            case 2:

                                System.out.println("Introduce el numero del pokemon a mostrar");
                                numPoke = entrada.nextInt();
                                System.out.println((numPoke) + bag.mostrarPokemon(numPoke - 1));
                                break;
                            case 3:
                                System.out.println(bag.Mochila.size());
                                boolean correcto = true;
                                do {
                                    System.out.println("Introduce el numero del pokemon a liberar");
                                    numPoke = entrada.nextInt();
                                    if (numPoke > bag.Mochila.size()) {
                                        System.out.println("Ingresa un número correcto");
                                        correcto = false;
                                    } else {
                                        System.out.println("El pokemon se ha eliminado correctamente");
                                        bag.liberarPokemon(numPoke);
                                        correcto = true;
                                    }
                                } while (correcto = false);
                                break;
                            case 4:
                                int captura,
                                 opcPokemon,
                                 chanceCaptura;
                                String respuesta;
                                System.out.println("Tienes "+bag.pokeballs+" pokeballs");
                                if (bag.pokeballs < 1) {
                                    System.out.println("No te quedan pokeballs");
                                } else {
                                    bag.pokeballs = bag.pokeballs - 1;
                                    chanceCaptura = (int) (Math.random() * 4);
                                    if (chanceCaptura >= 2) {
                                        bag.dinero = bag.dinero + 200;
                                        captura = (int) (Math.random() * 4);// 0 pikachu, 1 charmander, 2 pidgey, 3 squirtle
                                        int arrayAtributo[][];
                                        arrayAtributo = cargarAtributos();
                                        String[] pokedex = {"Pikachu", "Charmander", "Pidgey", "Squirtle"};
                                        System.out.println("Has atrapado un " + pokedex[captura] + " salvaje!!!");
                                        System.out.println("¿Quieres ponerle un mote a tu " + pokedex[captura] + "? si/no");
                                        do {
                                            respuesta = entrada.next().toLowerCase();
                                            if ((!respuesta.equals("si")) && (!respuesta.equals("no"))) {
                                                System.out.println("Introduce si/no");
                                            }
                                        } while ((!respuesta.equals("si")) && (!respuesta.equals("no")));
                                        //Switch que crea el pokemon que te ha tocado
                                        switch (captura) {
                                            case 0:
                                                Pikachu Pika;
                                                Pika = new Pikachu(arrayAtributo[0][0], arrayAtributo[0][1], arrayAtributo[0][2], arrayAtributo[0][3], arrayAtributo[0][4], arrayAtributo[0][5]);
                                                if (respuesta.equalsIgnoreCase("si")) {
                                                    String mote;
                                                    System.out.println("Escribe el mote de Pikachu");
                                                    mote = entrada.next();
                                                    Pika.setNombre(mote);
                                                }

                                                System.out.println("POKEMON CAPTURADO!!");
                                                System.out.println("-------------------");
                                                System.out.println("-------------------");
                                                System.out.println("Especie: " + Pika.getEspecie());
                                                System.out.println("Nivel: " + Pika.getNivel());
                                                System.out.println("Mote: " + Pika.getNombre());
                                                bag.registrarPokemon(Pika);
                                                break;
                                            case 1:
                                                Charmander Char;
                                                Char = new Charmander(arrayAtributo[1][0], arrayAtributo[1][1], arrayAtributo[1][2], arrayAtributo[1][3], arrayAtributo[1][4], arrayAtributo[1][5]);
                                                if (respuesta.equalsIgnoreCase("si")) {
                                                    String mote;
                                                    System.out.println("Escribe el mote de Charmander");
                                                    mote = entrada.next();
                                                    Char.setNombre(mote);
                                                }

                                                System.out.println("POKEMON CAPTURADO!!");
                                                System.out.println("-------------------");
                                                System.out.println("-------------------");
                                                System.out.println("Especie: " + Char.getEspecie());
                                                System.out.println("Nivel: " + Char.getNivel());
                                                System.out.println("Mote: " + Char.getNombre());
                                                bag.registrarPokemon(Char);
                                                break;
                                            case 2:
                                                Pidgey Pid;
                                                Pid = new Pidgey(arrayAtributo[2][0], arrayAtributo[2][1], arrayAtributo[2][2], arrayAtributo[2][3], arrayAtributo[2][4], arrayAtributo[2][5]);
                                                if (respuesta.equalsIgnoreCase("si")) {
                                                    String mote;
                                                    System.out.println("Escribe el mote de Pidgey");
                                                    mote = entrada.next();
                                                    Pid.setNombre(mote);
                                                }

                                                System.out.println("POKEMON CAPTURADO!!");
                                                System.out.println("-------------------");
                                                System.out.println("-------------------");
                                                System.out.println("Especie: " + Pid.getEspecie());
                                                System.out.println("Nivel: " + Pid.getNivel());
                                                System.out.println("Mote: " + Pid.getNombre());
                                                bag.registrarPokemon(Pid);
                                                break;
                                            case 3:
                                                Squirtle Squ;
                                                Squ = new Squirtle(arrayAtributo[3][0], arrayAtributo[3][1], arrayAtributo[3][2], arrayAtributo[3][3], arrayAtributo[3][4], arrayAtributo[3][5]);
                                                if (respuesta.equalsIgnoreCase("si")) {
                                                    String mote;
                                                    System.out.println("Escribe el mote de Squirtle");
                                                    mote = entrada.next();
                                                    Squ.setNombre(mote);
                                                }

                                                System.out.println("POKEMON CAPTURADO!!");
                                                System.out.println("-------------------");
                                                System.out.println("-------------------");
                                                System.out.println("Especie: " + Squ.getEspecie());
                                                System.out.println("Nivel: " + Squ.getNivel());
                                                System.out.println("Mote: " + Squ.getNombre());
                                                bag.registrarPokemon(Squ);
                                                break;
                                        }
                                    } else {
                                        System.out.println("No has conseguido capturar el pokemon");
                                    }

                                }

                                break;
                            case 5:
                                System.out.println("Cuántas pokeball quieres comprar? Tienes " + bag.dinero + " pokecuartos");
                                cantidad = entrada.nextInt();
                                boolean comprado;
                                comprado = bag.comprarPokeball(cantidad);
                                if (comprado == true) {
                                    System.out.println("Has comprado con éxito " + cantidad + " pokeballs");
                                    bag.dinero = bag.dinero - (100 * cantidad);
                                } else {
                                    System.out.println("Compra pokecuartos gañan");
                                }

                                break;
                            case 6:
                                //guardar

                                query = con.createStatement();
                                query.executeUpdate("delete from mochilas where nombre='" + player.nombre + "'");
                                query.executeUpdate("delete from pokemons where mochila='" + player.nombre + "'");

                                query.executeUpdate("INSERT INTO mochilas VALUE('" + player.nombre + "','" + player.genero + "',"+bag.dinero+","+bag.pokeballs+",'" + player.fecha + "')");
                                for (int i = 0; i < bag.Mochila.size(); i++) {
                                    query.executeUpdate("INSERT INTO pokemons VALUE('" + bag.Mochila.get(i).especie + "'," + bag.Mochila.get(i).nivel + ",'"+bag.Mochila.get(i).nombre+"',"+(i+1)+",'"+player.nombre+"')");
                                }

                                break;
                            case 7:
                                System.out.println("Salir");
                                salir=true;
                                break;
                        }

                    } catch (java.util.InputMismatchException ex) {
                        entrada.next();
                        System.out.println("Elige una opción del menú");
                    }
                } while (opc!=7);
            } catch (java.util.InputMismatchException ex) {
                entrada.next();
                System.out.println("");
            }
        } while (salir = false);

    }
/**
 * Funcion para cargar atributos del json
 * @return 
 */
    public static int[][] cargarAtributos() {
        // 0charmander 1bulbasaur 2squirtle 3pikachu
        int arrayAtributos[][] = new int[4][6];
        String nombreStats[] = {"ps", "ataque", "defensa", "atqEsp", "defEsp", "velocidad"};
        try {
            JsonParser parser = new JsonParser();
            Object obj = parser.parse(new FileReader("atributos.json"));
            JsonArray json = (JsonArray) obj;

            for (int i = 0; i < json.size(); i++) {

                JsonObject object = (JsonObject) json.get(i);
                for (int j = 0; j < nombreStats.length; j++) {
                    arrayAtributos[i][j] = Integer.parseInt(object.get(nombreStats[j]).toString());

                }

                System.out.println();

            }

        } catch (Exception ex) {
            System.out.println("Error :" + ex.getMessage());
        }
        return arrayAtributos;
    }
}
