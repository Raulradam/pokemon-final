/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicapokmon;

/**
 * 
 * @author dam
 */
public class Pidgey extends Pokemon{
    
    /**
     * 
     * @param ps
     * @param ataque
     * @param defensa
     * @param ataqueEsp
     * @param defensaEsp
     * @param velocidad 
     */
    public Pidgey(int ps, int ataque, int defensa, int ataqueEsp, int defensaEsp, int velocidad) { // estats base
        nombre = "PIDGEY";
        especie = "PIDGEY";
        this.nivel = (int) (Math.random() * 100);
        this.ps= (ps + (nivel * 2));
        this.ataque = (ataque + (nivel * 2));
        this.defensa = (defensa + (nivel * 3));
        this.atqEsp = (atqEsp + (nivel * 2));
        this.defEsp = (defEsp + (nivel * 3));
        this.velocidad = (velocidad + (nivel * 2));
        tipo = "VOLADOR";
            
    }
    /**
     * 
     * @param nivel 
     */
    public Pidgey(int nivel) {
        nombre = "PIDGEY";
        especie = "PIDGEY";
        this.nivel = nivel;
        this.ps= (ps + (nivel * 2));
        this.ataque = (ataque + (nivel * 2));
        this.defensa = (defensa + (nivel * 3));
        tipo = "VOLADOR";  
    }
    /**
     * 
     * @param nombre 
     */
    public void setNombre(String nombre){
        this.nombre=nombre;
    }
}
