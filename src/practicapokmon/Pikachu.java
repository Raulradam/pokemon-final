/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicapokmon;


import practicapokmon.Pokemon;

/**
 * 
 * @author dam
 */
//Constructor sin mote
public class Pikachu extends Pokemon{
    /**
     * 
     * @param nivel 
     */
      
    public Pikachu(int nivel) {
        nombre = "PIKACHU";
        especie = "PIKACHU";
        this.nivel = nivel;
        this.ps= (ps + (nivel * 2));
        this.ataque = (ataque + (nivel * 2));
        this.defensa = (defensa + (nivel * 3));
        tipo = "ELECTRICO";  
    }
    /**
     * 
     * @param ps
     * @param ataque
     * @param defensa
     * @param ataqueEsp
     * @param defensaEsp
     * @param velocidad 
     */
    public Pikachu(int ps, int ataque, int defensa, int ataqueEsp, int defensaEsp, int velocidad) { // estats base
        nombre = "PIKACHU";
        especie = "PIKACHU";
        this.nivel = (int) (Math.random() * 100);
        this.ps= (ps + (nivel * 2));
        this.ataque = (ataque + (nivel * 2));
        this.defensa = (defensa + (nivel * 3));
        this.atqEsp = (atqEsp + (nivel * 2));
        this.defEsp = (defEsp + (nivel * 3));
        this.velocidad = (velocidad + (nivel * 2));
        tipo = "ELECTRICO";
            
    }
    /**
     * setter devuelve nombre
     * @param nombre 
     */
    public void setNombre(String nombre){
        this.nombre=nombre;
    }

}